'use strict'

const r = require('rethinkdb')
const Promise = require('bluebird')
const uuid = require('uuid-base62')
const utils = require('./utils')

const defaults = {
  host: '192.168.99.100',
  port: 28015,
  db: 'insta-clone'
}

class Db {
  constructor (options = {}) {
    this.host = options.host || defaults.host
    this.port = options.port || defaults.port
    this.db = options.db || defaults.db
    this.setup = options.setup || false
  }

  connect (callback) {
    this.connection = r.connect({
      host: this.host,
      port: this.port
    })

    this.connected = true

    var db = this.db
    var connection = this.connection

    if (!this.setup) {
      return Promise.resolve(connection).asCallback(callback)
    }

    var setup = async function () {
      var conn = await connection

      var dbList = await r.dbList().run(conn)
      if (dbList.indexOf(db) === -1) {
        await r.dbCreate(db).run(conn)
      }

      var dbTables = await r.db(db).tableList().run(conn)
      if (dbTables.indexOf('images') === -1) {
        await r.db(db).tableCreate('images').run(conn)

        await r.db(db).table('images').indexCreate('createdAt').run(conn)
        await r.db(db).table('images').indexCreate('userId', { multi: true }).run(conn)
        await r.db(db).table('images').indexCreate('userLikes').run(conn)
      }

      if (dbTables.indexOf('users') === -1) {
        await r.db(db).tableCreate('users').run(conn)

        await r.db(db).table('users').indexCreate('username').run(conn)
      }

      return conn
    }

    return Promise.resolve(setup()).asCallback(callback)
  }

  disconnect (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    this.connected = false
    return Promise.resolve(this.connection)
      .then(conn => conn.close())
  }

  saveImage (image, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection

    var tasks = async function () {
      var conn = await connection
      image.createdAt = new Date()
      image.tags = utils.extractTags(image.description)

      var result = await r.db(db).table('images').insert(image).run(conn)

      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      image.id = result.generated_keys[0]

      await r.db(db).table('images').get(image.id).update({
        publicId: uuid.encode(image.id)
      }).run(conn)

      var created = await r.db(db).table('images').get(image.id).run(conn)

      return Promise.resolve(created)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  likeImage (id, username, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection
    var getImage = this.getImage.bind(this)

    var tasks = async function () {
      var conn = await connection

      var image = await getImage(id)

      await r.db(db).table('images').indexWait().run(conn)

      await r.db(db).table('images').get(image.id).update({
        userLikes: r.row('userLikes').default([]).append(username),
        likes: image.likes + 1
      }).run(conn)

      var updated = await getImage(id)

      return Promise.resolve(updated)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  dislikeImage (id, username, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection
    var getImage = this.getImage.bind(this)

    var tasks = async function () {
      var conn = await connection

      var image = await getImage(id)

      await r.db(db).table('images').indexWait().run(conn)

      await r.db(db).table('images').get(image.id).update({
        userLikes: r.row('userLikes').setDifference([username]),
        likes: image.likes - 1
      }).run(conn)

      var updated = await getImage(id)

      return Promise.resolve(updated)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImage (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection
    var imageId = uuid.decode(id)

    var tasks = async function () {
      var conn = await connection
      var image = await r.db(db).table('images').get(imageId).run(conn)

      if (!image) {
        return Promise.reject(new Error(`image ${imageId} not found`))
      }

      return Promise.resolve(image)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImages (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection

    var tasks = async function () {
      var conn = await connection

      await r.db(db).table('images').indexWait().run(conn)
      var images = await r.db(db).table('images').orderBy({ index: r.desc('createdAt') }).run(conn)

      var result = await images.toArray()

      return Promise.resolve(result)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  saveUser (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection

    var tasks = async function () {
      var conn = await connection
      if (!user.facebook) {
        user.password = utils.encrypt(user.password)
      }
      user.createdAt = new Date()

      var result = await r.db(db).table('users').insert(user).run(conn)
      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }

      user.id = result.generated_keys[0]

      var created = await r.db(db).table('users').get(user.id).run(conn)

      return Promise.resolve(created)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  getUser (username, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var db = this.db
    var connection = this.connection

    var tasks = async function () {
      var conn = await connection
      await r.db(db).table('users').indexWait().run(conn)

      var users = await r.db(db).table('users').getAll(username, { index: 'username' }).run(conn)
      var result = null

      try {
        result = await users.next()
      } catch (e) {
        return Promise.reject(new Error(`user ${username} not found`))
      }

      return Promise.resolve(result)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  authenticate (username, password, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var getUser = this.getUser.bind(this)

    var tasks = async function () {
      var user = null
      try {
        user = await getUser(username)
      } catch (e) {
        return Promise.resolve(false)
      }

      if (user.password === utils.encrypt(password)) {
        return Promise.resolve(true)
      }

      return Promise.resolve(false)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImagesByUser (userId, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var connection = this.connection
    var db = this.db

    var tasks = async function () {
      var conn = await connection

      await r.db(db).table('images').indexWait().run(conn)

      var images = await r.db(db).table('images').getAll(userId, { index: 'userId' }).orderBy(r.desc('createdAt')).run(conn)
      var result = await images.toArray()

      return Promise.resolve(result)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImagesByTag (tag, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }

    var connection = this.connection
    var db = this.db
    tag = utils.normalize(tag)

    var tasks = async function () {
      var conn = await connection

      await r.db(db).table('images').indexWait().run(conn)

      var images = await r.db(db).table('images').filter((img) => {
        return img('tags').contains(tag)
      }).orderBy(r.desc('createdAt')).run(conn)

      var result = await images.toArray()

      return Promise.resolve(result)
    }

    return Promise.resolve(tasks()).asCallback(callback)
  }
}

module.exports = Db
