'use strict'

const crypto = require('crypto')

var extractTags = text => {
  if (text == null) return []

  var matches = text.match(/#(\w+)/g)

  if (matches === null) return []

  // Programación funcional
  matches = matches.map(normalize)

  return matches
}

function normalize (text) {
  text = text.toLowerCase()
  text = text.replace(/#/g, '')
  return text
}

function encrypt (password) {
  var shasum = crypto.createHash('sha256')
  shasum.update(password)
  return shasum.digest('hex')
}

const utils = {
  extractTags,
  encrypt,
  normalize
}

module.exports = utils
