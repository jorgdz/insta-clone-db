'use strict'

const test = require('ava')
const r = require('rethinkdb')
const uuid = require('uuid-base62')

const Db = require('../lib/db')
const fixtures = require('./fixtures')
const utils = require('../lib/utils')

test.beforeEach('setup database', async t => {
  const dbName = `instagram_${uuid.v4()}`
  const db = new Db({ db: dbName, setup: true })
  await db.connect()

  t.context.db = db
  t.context.dbName = dbName

  t.true(db.connected, 'should be connected')
})

// Borrar la base de datos despues de cada test
test.afterEach.always('cleanup database', async t => {
  var db = t.context.db
  var dbName = t.context.dbName

  await db.disconnect()
  t.false(db.connected, 'should be disconnected')

  var conn = await r.connect({
    host: '192.168.99.100',
    port: 28015
  })

  await r.dbDrop(dbName).run(conn)
})

test('save image', async t => {
  var db = t.context.db

  t.is(typeof db.saveImage, 'function', 'saveImage is function')

  var image = fixtures.getImage()

  var created = await db.saveImage(image)
  t.is(created.description, image.description)
  t.is(created.url, image.url)
  t.is(created.likes, image.likes)
  t.is(created.liked, image.liked)

  t.deepEqual(created.tags, ['awesome', 'tags', 'instagram'])

  t.is(created.userId, image.userId)
  t.is(typeof created.id, 'string')

  t.is(created.publicId, uuid.encode(created.id))

  t.truthy(created.createdAt)
})

test('like image', async t => {
  var db = t.context.db

  t.is(typeof db.likeImage, 'function', 'likeImage is a function')

  var image = fixtures.getImage()
  var created = await db.saveImage(image)
  var result = await db.likeImage(created.publicId, image.userId)

  t.is(result.likes, image.likes + 1)
})

test('get image', async t => {
  var db = t.context.db

  t.is(typeof db.getImage, 'function', 'getImage is a function')

  var image = fixtures.getImage()
  var created = await db.saveImage(image)
  var result = await db.getImage(created.publicId)

  t.deepEqual(created, result)

  await t.throwsAsync(async () => {
    await db.getImage('foo')
  })
})

test('list all images', async t => {
  var db = t.context.db

  var images = fixtures.getImages(3)
  var saveImages = images.map(img => db.saveImage(img))
  var created = await Promise.all(saveImages)
  var result = await db.getImages()

  t.is(created.length, result.length)
})

test('save user', async t => {
  var db = t.context.db

  t.is(typeof db.saveUser, 'function', 'saveUser is a function')

  var user = fixtures.getUser()
  var plainPassword = user.password
  var created = await db.saveUser(user)

  t.is(user.username, created.username)
  t.is(user.email, created.email)
  t.is(user.name, created.name)
  t.is(utils.encrypt(plainPassword), created.password)
  t.is(typeof created.id, 'string')
  t.truthy(created.createdAt)
})

test('get user', async t => {
  var db = t.context.db

  t.is(typeof db.getUser, 'function', 'getUser is a function')

  var user = fixtures.getUser()
  var created = await db.saveUser(user)
  var result = await db.getUser(created.username)

  t.deepEqual(created, result)

  await t.throwsAsync(async () => {
    await db.getUser('foo')
  })
})

test('authenticate user', async t => {
  var db = t.context.db

  t.is(typeof db.authenticate, 'function', 'authenticate is a function')

  var user = fixtures.getUser()
  var plainPassword = user.password
  await db.saveUser(user)

  var success = await db.authenticate(user.username, plainPassword)
  t.true(success)

  var fail = await db.authenticate(user.username, 'foo')
  t.false(fail)

  var failure = await db.authenticate('foo', 'bar')
  t.false(failure)
})

test('list images by user', async t => {
  var db = t.context.db

  t.is(typeof db.getImagesByUser, 'function', 'getImagesByUser is a function')

  var images = fixtures.getImages(10)
  var userId = uuid.uuid()
  var random = Math.round(Math.random() * images.length)

  var saveImages = []
  for (var i = 0; i < images.length; i++) {
    if (i < random) {
      images[i].userId = userId
    }

    saveImages.push(db.saveImage(images[i]))
  }

  await Promise.all(saveImages)

  var result = await db.getImagesByUser(userId)
  t.is(result.length, random)
})

test('list images by tags', async t => {
  var db = t.context.db

  t.is(typeof db.getImagesByTag, 'function', 'getImagesByTag is a function')

  var images = fixtures.getImages(10)
  var tag = '#filterit'
  var random = Math.round(Math.random() * images.length)

  var saveImages = []
  for (var i = 0; i < images.length; i++) {
    if (i < random) {
      images[i].description = tag
    }

    saveImages.push(db.saveImage(images[i]))
  }

  await Promise.all(saveImages)

  var result = await db.getImagesByTag(tag)
  t.is(result.length, random)
})
